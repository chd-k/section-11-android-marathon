package com.example.updatedpersonalinfoapplication

import android.content.Context

private const val APP_PREF = "PersonalInfoApp"
private const val APP_PREF_FIRST_NAME = "PersonalInfoAppFirstName"
private const val APP_PREF_SECOND_NAME = "PersonalInfoAppSecondName"
private const val APP_PREF_LAST_NAME = "PersonalInfoAppLastName"
private const val APP_PREF_AGE = "PersonalInfoAppAge"
private const val APP_PREF_HOBBY = "PersonalInfoAppHobby"

object AppSharedPreferences {
    fun setData(context: Context, personInfo: PersonInfo) {
        context
            .getSharedPreferences(APP_PREF, Context.MODE_PRIVATE)
            .edit()
            .putString(APP_PREF_FIRST_NAME, personInfo.firstName)
            .putString(APP_PREF_SECOND_NAME, personInfo.secondName)
            .putString(APP_PREF_LAST_NAME, personInfo.lastName)
            .putInt(APP_PREF_AGE, personInfo.age)
            .putString(APP_PREF_HOBBY, personInfo.hobby)
            .apply()
    }

    fun getData(context: Context): PersonInfo {
        val preferences = context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE)
        return PersonInfo(
            preferences.getString(APP_PREF_FIRST_NAME, null)!!,
            preferences.getString(APP_PREF_SECOND_NAME, null)!!,
            preferences.getString(APP_PREF_LAST_NAME, null)!!,
            preferences.getInt(APP_PREF_AGE, 0),
            preferences.getString(APP_PREF_HOBBY, null)!!)
    }

    fun isUsed(context: Context): Boolean =
        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).contains(
            APP_PREF_FIRST_NAME)
}