package com.example.updatedpersonalinfoapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider

class NewActivity : AppCompatActivity() {
    private lateinit var buttonCounter: Button
    private lateinit var textCounter: TextView
    private lateinit var viewModel: AppViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)
        buttonCounter = findViewById(R.id.buttonCounter)
        textCounter = findViewById(R.id.textViewCounter)
        viewModel = ViewModelProvider(this).get(AppViewModel::class.java)
        viewModel.currentState.observe(this) { textCounter.text = it.toString() }
        buttonCounter.setOnClickListener {
            viewModel.currentState.value = ++viewModel.counter
        }
    }
}