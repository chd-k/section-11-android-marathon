package com.example.updatedpersonalinfoapplication

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AppViewModel : ViewModel() {
    var counter = 0
    val currentState: MutableLiveData<Int> by lazy {
        MutableLiveData<Int>()
    }
}