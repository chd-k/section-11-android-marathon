package com.example.updatedpersonalinfoapplication

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class PersonInfo(
    val firstName: String,
    val secondName: String,
    val lastName: String,
    val age: Int,
    val hobby: String,
) : Parcelable