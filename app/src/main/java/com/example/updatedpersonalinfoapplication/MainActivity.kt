package com.example.updatedpersonalinfoapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat

class MainActivity : AppCompatActivity() {
    private lateinit var buttonDone: Button
    private lateinit var textFirstName: EditText
    private lateinit var textSecondName: EditText
    private lateinit var textLastName: EditText
    private lateinit var textAge: EditText
    private lateinit var textHobby: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textFirstName = findViewById(R.id.editTextFirstName)
        textSecondName = findViewById(R.id.editTextSecondName)
        textLastName = findViewById(R.id.editTextLastName)
        textAge = findViewById(R.id.editTextAge)
        textHobby = findViewById(R.id.editTextHobby)
        if (AppSharedPreferences.isUsed(this@MainActivity)) {
            val personInfo = AppSharedPreferences.getData(this@MainActivity)
            textFirstName.setText(personInfo.firstName, TextView.BufferType.EDITABLE)
            textSecondName.setText(personInfo.secondName, TextView.BufferType.EDITABLE)
            textLastName.setText(personInfo.lastName, TextView.BufferType.EDITABLE)
            textAge.setText(personInfo.age.toString(), TextView.BufferType.EDITABLE)
            textHobby.setText(personInfo.hobby, TextView.BufferType.EDITABLE)
        }
        buttonDone = findViewById(R.id.buttonDone)
        buttonDone.setOnClickListener {
            val person = PersonInfo(
                textFirstName.text.toString(),
                textSecondName.text.toString(),
                textLastName.text.toString(),
                textAge.text.toString().toInt(),
                textHobby.text.toString()
            )
            InfoActivity.newIntent(this@MainActivity, person).also {
                startActivity(it)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.save_entries -> {
                AppSharedPreferences.setData(this@MainActivity, PersonInfo(
                    textFirstName.text.toString(),
                    textSecondName.text.toString(),
                    textLastName.text.toString(),
                    textAge.text.toString().toInt(),
                    textHobby.text.toString()))
            }
            R.id.change_background -> {
                val constraintLayout: ConstraintLayout = findViewById(R.id.constraintLayout)
                val id = when ((1..6).random()) {
                    1 -> R.drawable.wallpaper01
                    2 -> R.drawable.wallpaper02
                    3 -> R.drawable.wallpaper03
                    4 -> R.drawable.wallpaper04
                    5 -> R.drawable.wallpaper05
                    else -> R.drawable.wallpaper06
                }
                constraintLayout.background = ResourcesCompat.getDrawable(resources, id, null)
            }
            R.id.next_screen -> {
                startActivity(Intent(this, NewActivity::class.java))
            }
        }
        return super.onOptionsItemSelected(item)
    }
}