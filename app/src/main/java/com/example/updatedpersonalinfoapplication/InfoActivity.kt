package com.example.updatedpersonalinfoapplication

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

private const val extraKey = "com.example.personalinfoapplication.person"

class InfoActivity : AppCompatActivity() {
    private lateinit var buttonBack: Button
    private lateinit var personInfo: PersonInfo
    private lateinit var textHello: TextView
    private lateinit var textThanks: TextView
    private lateinit var textHobbies: TextView
    private lateinit var textAgeTitle: TextView
    private lateinit var textAgeInfo: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        personInfo = intent.getParcelableExtra(extraKey)!!
        textHello = findViewById(R.id.textViewHello)
        textThanks = findViewById(R.id.textViewThanks)
        textHobbies = findViewById(R.id.textViewHobbies)
        textAgeTitle = findViewById(R.id.textViewAgeTitle)
        textAgeInfo = findViewById(R.id.textViewAgeInfo)
        textHello.text = getString(R.string.text_hello, personInfo.firstName, personInfo.lastName)
        textThanks.text = getString(R.string.text_thanks)
        textHobbies.text = personInfo.hobby
        textAgeTitle.text = getString(R.string.text_age_title, personInfo.age)
        textAgeInfo.text = when (personInfo.age) {
            in 122..139 -> getString(R.string.text_age_info_lost)
            in 98..121 -> getString(R.string.text_age_info_greatest)
            in 80..97 -> getString(R.string.text_age_info_silent)
            in 62..79 -> getString(R.string.text_age_info_baby_boomers)
            in 41..61 -> getString(R.string.text_age_info_gen_x)
            in 18..40 -> getString(R.string.text_age_info_millennials)
            in 0..17 -> getString(R.string.text_age_info_gen_z)
            else -> getString(R.string.text_age_info_wrong)
        }
        buttonBack = findViewById(R.id.buttonBack)
        buttonBack.setOnClickListener {
            finish()
        }
    }

    companion object {
        fun newIntent(context: Context, personInfo: PersonInfo): Intent {
            return Intent(context, InfoActivity::class.java).apply {
                putExtra(extraKey, personInfo)
            }
        }
    }
}